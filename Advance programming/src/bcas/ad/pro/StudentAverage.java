package bcas.ad.pro;

public class StudentAverage {
	
	static int total = 0;
	static double average = 0;
	
	private static void findTotal(int array[]) {
		
		for(int i = 0; i < array.length; i++) {
			total = total + array[i];
		}
	}
	
    private static void findAverage(double length) {
		
		average = total/length;
		}
	
	private static void displayMarks(int array[]) {
		
		for(int i = 0; i < array.length; i++) {
			System.out.println(array[i] + ",");
		}
	}
 private static void displayResults() {
		System.out.println(" Total is : " + total);
		System.out.println(" Average is : " + average);
		}
	
 
 public static void main(String args[]) {
		
		
		int [] marks = {70,75,80,90,65};
		
		findTotal(marks);
		findAverage(marks.length);
		displayMarks(marks);
		displayResults();
	}

}
