package bcas.ad.pro;


public class StudentStaticBlock {

	static {//static block will invoke before main method
		
		System.out.println("Hi from static block");
	}
	
	
	public static void main(String[] args) {
		
		
		
		 System.out.println("Hi from Main");
	}
}
